const slider = document.getElementById("userRange");
const output = document.getElementById("selectedCount");
const freeCard = document.getElementById("freeCard");
const proCard = document.getElementById("proCard");
const enterpriseCard = document.getElementById("enterpriseCard");
const cardElements = [freeCard, proCard, enterpriseCard];
const characterContainer = document.getElementById("characterContainer");
output.innerHTML = slider.value;
add_card_highlighting(freeCard);

slider.oninput = function () {
    let userCount = this.value;
    output.innerHTML = userCount;

    if (userCount <= 10)
        add_card_highlighting(freeCard);
    else if (userCount > 10 && this.value <= 20)
        add_card_highlighting(proCard);
    else
        add_card_highlighting(enterpriseCard);
}

function add_card_highlighting(element) {
    if (!element) return;
    cardElements.forEach((cardElement) => {
        cardElement.classList.remove("gradient-card");
    });
    element.classList.add("gradient-card", "shadow-lg");
}

function remove_highlight(elements) {
    elements.forEach((element) => {
        if (!element) return;
        element.classList.remove("shadow-lg");
    });
}

const url = 'https://random-data-api.com/api/users/random_user?';
let characterLimit = 8;

function fetchCharacters() {

    try {
        fetch(`${url}size=${characterLimit}`)
            .then(response => response.json())
            .then(data => {

                console.log('data ', data)
                data.forEach((character) => {
                    let characterElement = document.createElement("div");
                    characterElement.classList.add("character", "d-flex", "gy-5", "flex-column", "rounded", "p-4", "mb-4");
                    characterElement.innerHTML = `
                  <img src="${character.avatar}" class="card-img-top rounded card-img" alt="avatar"/>
                  <div class="card-body mt-4 character-detail">
                    <div class="d-flex justify-content-between">
                        <h5 class="card-title">${character.first_name}</h5>
                        <span class="">${character.date_of_birth}</span>
                    </div>
                    <br/>
                    <p>Phone: ${character.phone_number}</p>
                    <span>Email: ${character.email}</span>
                    <p>Gender: ${character.gender}</p>
                    <span class="badge badge-pill badge-primary">
                        ${character.employment.title}
                    </span>
                  </div>
                `;
                    characterContainer.appendChild(characterElement);
                });
            })
    } catch (error) {
        console.error(error);
    }
}

fetchCharacters();

const characterEnd = document.querySelector("#characterEnd");
const options = {
    root: null,
    rootMargin: "10px",
    threshold: 1
};
const myFirstObserver = new IntersectionObserver((elements) => {
    elements.forEach((element) => {
        if (element.isIntersecting) {
            setTimeout(() => {
                fetchCharacters();

            }, 3000);
        }
    });
}, options);

myFirstObserver.observe(characterEnd);